#!/bin/bash
# Name: build-deb.sh
# Short-Description: build deb package from a *.tar.gz file
# Version: 1.0.1

# Devs: Pavel Sibal <entexsoft@gmail.com>
#
# build-deb.sh is script for help with building deb packages from a *.tar.gz
# file. The file must to be prepared for this and must to contain DEBIAN folder
# with a control script.
#
# build-deb.sh is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# You can get a copy of the license at www.gnu.org/licenses
#
# build-deb.sh is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# <http://www.gnu.org/licenses/>.

# build-deb.sh [package_name] [package_version] [architecture]

help() {
  if [ -n "$1" ]; then
    echo -e "\n${1}\n"
  fi
  cat << EOF
SYNOPSIS
    ${0} [package_name] [package_version] [architecture]

CHOICES
    package_name
      is name of the debian package. The name of the compressed file (*.tar.gz)
      must to be the same like the name of the package (without the tar.gz).

    package_version
      the version must to be three numbers with a dots (example: 1.0.0). This
      must to be the same numbers like is set in the DEBIAN/control.

    architecture
      architecture is a optional argument. if is not set, then the architecture
      will be set at 'all'. Acceptable architectures is: amd64, arm64, armel,
      armhf, i386, mips, mipsel, mips64el, ppc64el, s390x.

EOF
}

if [ "$UID" -ne "0" ]; then
  help "!!! YOU MUST BE ROOT TO RUN THIS SCRIPT !!!"
  exit 1
fi

if [ -z "$1" ]; then
  help "!!! THE FIRST ARGUMENT MUST TO BE NAME OF THE PACKAGE !!!"
  exit 1
fi

if [ ! -d ${1} ]; then
  help "!!! THE DIRECTORY NOT EXISTS !!!"
  exit 1
fi

if [ -z "$2" ]; then
  help "!!! THE SECOND ARGUMENT MUST TO BE VERSION OF THE PACKAGE !!!"
  exit 1
fi

if [ -z "$3" ]; then
  ARCH="all"
else
  ARCH=$3
fi

echo -e "BUILDING PACKAGE: $1_$2_${ARCH}.deb"
OWNER=$($(which stat) -c '%U:%G' ${1}/)
$(which chown) -hR root:root ${1}/
$(which chmod) -R 0755 ${1}/
$(which chmod) 0444 ${1}/DEBIAN/control
if [ -d ${1}/usr/share/doc/${1} ]; then
  $(which chmod) 0644 ${1}/usr/share/doc/${1}/*
fi
if [ -d ${1}/usr/share/desktop-base/${1} ]; then
  $(which chmod) 0644 ${1}/usr/share/desktop-base/${1}/grub/*
  $(which chmod) 0644 ${1}/usr/share/desktop-base/${1}/lockscreen/contents/images/*
  $(which chmod) 0644 ${1}/usr/share/desktop-base/${1}/login/*
  $(which chmod) 0644 ${1}/usr/share/desktop-base/${1}/wallpaper/contents/images/*
fi
if [ -d ${1}/usr/share/applications ]; then
  $(which chmod) 0644 ${1}/usr/share/applications/*
fi
if [ -d ${1}/usr/share/bash-completion/completions ]; then
  $(which chmod) 0644 ${1}/usr/share/bash-completion/completions/*
fi
$(which find) * -type f ! -regex '^DEBIAN/.*' -exec md5sum {} \; > ${1}/DEBIAN/md5sums
$(which chmod) -R 0444 ${1}/DEBIAN/md5sums
$(which dpkg-deb) -b ${1}/ ${1}_${2}_${ARCH}.deb
$(which chown) -f $OWNER *.deb
$(which chmod) a+rw *.deb
$(which chown) -hR $OWNER ${1}/
$(which rm) -f ${1}/DEBIAN/md5sums
echo -e "BUILDING PACKAGE: done"
exit 0
