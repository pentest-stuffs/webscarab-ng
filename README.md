# WebScarab NG

WebScarab-NG is a complete rewrite of the old WebScarab application, with a special focus on making the application more user-friendly. To this end, WebScarab-NG makes use of the Spring Rich Client Platform to provide the user interface features. By using the Spring Rich Client Platform, WebScarab-NG automatically gains things like default buttons, keyboard shortcuts, support for internationalisation, etc.

Another new feature is that session information is now written into a database, rather than into hundreds or thousands of individual files. This makes disk space utilisation and things like archiving of sessions a lot easier.

Ultimately, WebScarab-NG will have all the significant functionality that the old WebScarab had, although it will be reorganised quite significantly, in order to make the application more user friendly.


WebScarab NG was tested in this linux distributions:
* Debian Stretch
* Linux Mint

### Links:
* Homepage: https://www.owasp.org/index.php/OWASP_WebScarab_NG_Project



## INSTALLATION

### Dependencies:
WebScarab NG is a java tool, which need for a correct running a few another
packages.

If you will install this by the `dpkg -i`, then make sure, that you have
installed all the depending packages:

```
apt-get install default-jre openjdk-8-jre
```

### Install from source:
Unpack the source package and run command (like root):

```
make install
```

### Uninstall from source:
Run command (like root):
```
make uninstall
```

### Build Debian package
Run command (like root):
```
make build-deb
```

### Install from *.deb package:
```
dpkg -i webscarab-ng*.deb
```
